# Input
# input() allows us to gather data from the user input,
# returns "string" data type
# "\n" stands for line break

# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course.")

# num1 = input("enter number: \n")
# num2 = input("enter number: \n")
# print(f"{num1+num2}")

# If-else statements
# used to choose between 2 or more code blocks
# depending on condition

# Declare a variable to use for the conditional statement

# test num = 75

# if test_num >= 60:
# 	print("Test Passed.")
# else:
# 	print("Test failed.")

# Else-if chains

# num5 = int(input("enter number: \n"))

# if num5 % 5 == 0 and num5 % 3 == 0:
# 	print("The number is divisible by 3 and 5")
# elif num5 % 3 == 0:
# 	print("The number is divisible by 3")
# elif num5 % 5 == 0:
# 	print("The number is divisible by 5")
# else:
# 	print("The number is not divisible by 3 nor 5")

# for x in range(6, 10, 2):
# 	print(f"The current value is {x}")

# Break statements
# # The break statement is used to stop the loop
# j=1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1

# Continue statement
# it returns

k = 1
while k < 6:
	k += 1
	if k == 3:
		continue
	print(k)